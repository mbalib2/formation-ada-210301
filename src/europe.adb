with Ada.Text_IO; use Ada.TExt_IO;

package body Europe is

   procedure Faire_Espagne is

      type Situation is (Maritime, Terrestre);
      type Emplacement is record
         longitude : Float;
         latitude : Float;
         endroit : Situation;
         -- en + : nom de la ville la + proche
         ville : string (1 .. 15);
      end record;

      e1_madrid:Emplacement:=(-1.0, 37.2, Terrestre, "Madrid         ");

   begin
      Put_Line("Espagne:");
      Put_Line("Madrid : " & e1_madrid.longitude'Image &
                  " " & e1_madrid.latitude'Image &
                  " " & e1_madrid.endroit'Image &
                  " " & e1_madrid.ville);
   end Faire_Espagne;

   procedure Faire_Italie is

      package Destination is
         type Instance is tagged record
            nom : string(1..10);
            pays : string(1..2);
            jours : positive;
         end record;
         function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1) return Instance;
         procedure Afficher(self : in out Instance);
         procedure Allonger(self : in out instance ; ajout : integer);
         procedure Racourcir(self : in out instance ; enleve : integer);
         function Heures(self : instance) return positive;
         procedure AfficherAvecEtoiles(self : in out instance'class);
      end Destination;

      package body Destination is
         function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1) return Instance is
            i:instance:=(nom, pays, jours);
         begin
            return i;
         end Create;
         procedure Afficher(self : in out Instance) is
         begin
            Put_Line(self.nom & self.pays & self.jours'Image);
         end Afficher;
         procedure Allonger(self : in out Instance ; ajout : integer) is
         begin
            self.jours := self.jours + ajout;
         end Allonger;
         procedure Racourcir(self : in out Instance ; enleve : integer) is
         begin
            self.Allonger(-enleve);
         end Racourcir;
         function Heures(self : instance) return positive is
         begin
            return 24 * self.jours;
         end;
         procedure AfficherAvecEtoiles(self : in out instance'class) is
         begin
            Put_Line("**********");
            self.Afficher;
            Put_Line("**********");
         end AfficherAvecEtoiles;

      end Destination;

      package Destination_Maritime is
         type Instance is new Destination.Instance with record
            ile: string (1..10);
         end record;
         overriding function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1) return Instance ;

         function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1 ;
                         ile : string := "           ") return Instance ;
         overriding procedure Afficher(self : in out Instance);
      end Destination_Maritime;

      package body Destination_Maritime is
         overriding function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1) return Instance is
            i:instance:=(nom, pays, jours, "          ");
         begin
            return i;
         end Create;

         function Create(nom : string := "          ";
                         pays : string := "??";
                         jours : positive := 1 ;
                         ile : string := "           ") return Instance is
            i:instance:=(nom, pays, jours, ile);
         begin
            return i;
         end Create;

         overriding procedure Afficher(self : in out Instance) is
         begin
            -- 3 solutions :
            -- Put_Line(self.nom & self.pays & self.jours'Image);
            -- Destination.Afficher(Destination.Instance(self));
            Destination.Instance(self).Afficher;
            Put_Line(" - " & self.ile);
         end Afficher;
      end Destination_Maritime;

      d1 : Destination.Instance := Destination.Create("Venise    ","IT", 4);
      dm1 : Destination_Maritime.Instance :=
         Destination_Maritime.Create("Palermo   ", "IT", 3, "Sicile    ");
      d2 : Destination.Instance := d1;
   begin
      Put_Line("Italie:");
      -- Destination.Afficher(d1);
      d1.Afficher;
      d1.Allonger(2);
      d1.Racourcir(3);
      d1.Afficher;
      Put_Line("En heures : " & d1.heures'Image); -- appel de fonction !
      dm1.Afficher; -- avec affichage de l'ile
      d1.AfficherAvecEtoiles; -- app�le finalement D.Afficher
      dm1.AfficherAvecEtoiles; -- appele bien finalement DM.Afficher !

      d1.Allonger(10);
      d2.Afficher;
   end;

end Europe;
