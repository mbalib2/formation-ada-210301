with Ada.Text_IO ; use Ada.Text_IO;
with Ada.Numerics.Discrete_Random;
with Ada.Containers.Vectors;

package body Oceanie is
   procedure faire_Australie is

      type tloterienum is range 1..20;
      package rloterie is new Ada.Numerics.Discrete_Random(tloterienum);
      genloterie : rloterie.generator;
      package vloterie is new Ada.Containers.Vectors(Index_Type=>Natural,
                                                     Element_Type=>tloterienum);
      v:vloterie.vector;

      task tirage;

      task affichage is
         entry demarrage;
      end affichage;

      task body tirage is
         num: tloterienum;
      begin
         Put_Line("Debut tirage");

         -- tirer 4 num�ro (boucle)
         -- a chaque fois, si le num�ro a d�j� �t� tir�, en tirer un nouveau (avec Pause)
         while integer(v.length)<4 loop
            delay 1.0;
            num := rloterie.random(genloterie);
            if not v.contains(num) then
               v.append(num);
               Put_Line("Tirage : " & num'Image);
            else
               Put(".");
            end if;
         end loop;
         affichage.demarrage;
         Put_Line("Fin tirage");
      end tirage;

      task body affichage is
      begin
         accept demarrage;
         Put_Line("Resultat : ");
         for i in 0..3 loop Put_Line(" - " & tloterienum'Image(v(i))); end loop;
         Put_Line("Fin Australie");
      end;
   begin
      Put_Line("Australie : ");
      rloterie.reset(genloterie);
   end Faire_Australie;
end Oceanie;
