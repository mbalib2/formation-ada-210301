with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Strings.Fixed;
with Ada.Text_IO.Unbounded_IO;
with Ada.Calendar; use Ada.Calendar;
with Ada.Calendar.Formatting;
with Ada.Containers.Vectors;

Package body Asie is
   procedure Faire_Russie is
      dest, tel : string (1..10) := "          ";
      nom : Unbounded_String;
      dest_l, tel_l : natural;
      maintenant : Ada.Calendar.Time := Ada.Calendar.Clock;
      preparation : duration;
      depart : Ada.Calendar.Time;
      f : File_Type; -- Ada.Text_IO
   begin
      Put_Line("Russie : ");
      Put_Line("Destination ?");
      Get_Line(dest, dest_l);
      Put_Line("Nom ?");
      Ada.Text_IO.Unbounded_IO.Get_Line(nom);
      Put_Line("Telephone ?");
      Get_Line(tel, tel_l);

      -- Corriger nom et dest : "   Moscou " => "Moscou"
      nom := Ada.Strings.Unbounded.Trim(nom, Ada.Strings.both);

      dest_l := Ada.Strings.Fixed.Trim(dest, Ada.Strings.both)'Length;
      dest(1..dest_l) := Ada.Strings.Fixed.Trim(dest, Ada.Strings.both);

      Put_Line("Voyage pour " & dest(1..dest_l) & " enregistre " &
                  "pour " & To_String(nom) & ". " );

      Put_Line("Annee en cours :" & Ada.Calendar.Year(maintenant)'Image);
      Put_Line("Maintenant :" & Ada.Calendar.Formatting.Image(maintenant));
      -- maintenant + d => time
      -- maintenant - d => time
      -- d + d => duration
      -- d - d => duration
      -- maintenant - maintenant => duration
      -- maintenant + maintenant => impossible

      -- Aficher : depart au mieux le ../.. (4 semaines et 3j + tard)
      preparation := duration((7*4+3)*24*60*60);
      depart := maintenant + preparation;
      Put_Line("Depart au mieux le : " & Ada.Calendar.Day(depart)'Image & "/"&
                  Ada.Calendar.Month(depart)'Image);

      -- fabrique un fichier russie.csv, chaque ligne ressemble � :
      -- Moscou,Bob,09239238942
      begin
         Open(f, Append_File, "russie.csv");
      exception
         when name_error => Create(f, Out_File, "russie.csv");
      end;
      Put_Line(f, dest & "," & To_String(nom) & "," & tel);
      Close(f);
   end Faire_Russie;

   procedure Faire_Japon is
      type tage is range 0..120;
      a_i : integer;
      a, min_age, max_age : tage;
      package tvectage is new Ada.Containers.Vectors(Index_Type=>Natural,
                                                  Element_Type=>tage);
      v:tvectage.vector;
   begin
      Put_Line("Japon :");
      -- demander l'age des voyageurs, et stoquer dans un vecteur
      -- quand on rentre un 0, on sort et on relit le vecteur pour
      -- indiquer � la fin le + jeune et le plus vieux
      loop
         Get(a_i);
         a:=tage(a_i);
         exit when a = 0;
         v.append(a);
      end loop;
      min_age := 120;
      max_age := 0;
      for i in 0 .. natural(v.length)-1 loop
         if min_age > v(i) then min_age := v(i); end if;
         if max_age < v(i) then max_age := v(i); end if;
      end loop;

      Put_Line("Intervale des ages : " & min_age'Image & "-" & max_age'Image);

   end;

end Asie;
